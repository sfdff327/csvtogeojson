program csv2geojson
implicit none
integer, parameter :: ascii = selected_char_kind ("ascii")

integer*4 Easc,eeasc,Dasc,ddasc,pasc,masc

integer*4 Ni,Nj,i,j,k,ierror
integer*4 wordLen
integer*4 NB

character(kind=ascii, len=256) :: pw
integer*4 pwLen


character(kind=ascii, len=512) :: vacancy,word,tempword
character(kind=ascii, len=256) :: infile
integer*4 xID,yID
integer*4 no_del_column
integer*4, allocatable:: NB_column(:)
character(kind=ascii, len=256), allocatable:: csvWord(:),title(:)

!inquire(unit = u, exist = unit_available)  �P�_�ɮ׬O�_�s�b

!!!!!!!!!!!!!!!!
integer*4 no_column,no_line
integer*4 local_no_column
integer*4 no_cal,scale1,scale2
integer*4 ss,ee,QQ,WW
!!!!!!!!!!!!!!!!

integer*4 INB
real*8 DNB

eeasc=iachar('e')
Easc=iachar('E')
Dasc=iachar('D')
ddasc=iachar('d')
pasc=iachar('+')
masc=iachar('-')

scale1=512
scale2=256

open(20,file='data.json', encoding='utf-8')
call geojsonstart(20)

do i=1,512
    write(vacancy(i:i),"(A1)") ' '
end do

pwLen=len(pw)
pw=repeat(' ',pwLen)

pw='Program CsvToGeoJSON:nichuenf@gmail.com,sfdff327@gmail.com'
pwLen=len(trim(pw))

open(10,file='sp.dat')
read(10,"(A256)") word
wordLen=len(trim(word))
if (pwLen/=wordLen) then
    write(*,*) 'Please mail to nichuenf@gmail.com,sfdff327@gmail.com'
    stop
else
    do i=1,wordLen
        if (iachar(pw(i:i))/=iachar(word(i:i))) then
            write(*,*) 'Please mail to nichuenf@gmail.com,sfdff327@gmail.com'
            stop
        end if
    end do
end if
read(10,"(A256)") infile
read(10,*) xID
read(10,*) yID
!read(10,*) no_del_column
!allocate(NB_column(no_del_column))
!do i=1,no_del_column
!    read(10,*) NB_column(i)
!end do
close(10)

no_column=0
open(10,file=infile)
read(10,"(A512)") word
do i=1,512
    if (word(i:i)==',') then
        no_column=no_column+1
        NB=i
    end if
    if (word(i+1:512)==vacancy(i+1:512)) then
        if (word(i:i)/=',') then
            no_column=no_column+1
        end if
        goto 121
    end if
end do

121 i=0
 
allocate(csvWord(no_column),title(no_column))
title=''
call readcsvword(vacancy,word,no_column,title)
no_line=0
do while(.not. eof(10))
    read(10,"(A256)") word
    if (word(1:5)/='     ') no_line=no_line+1
end do
close(10)

open(10,file=infile)
read(10,"(A512)") word
do Ni=1,no_line
    csvWord=''
    read(10,"(A512)") word
    
    !if (NI==129) then
    !    k=0
    !end if
    
    
    !read(word(1:512),*,IOSTAT=ierror) (csvWord(i),i=1,no_column)
    call readcsvword(vacancy,word,no_column,csvWord)
    
    write(20,"(A5)") '    {'  
    write(20,"(A23)") '     "type": "Feature",'
    write(20,"(A20)") '     "properties": {'
    
    ss=iachar(' ')
    
    do i=no_column,1,-1
        if (i/=xID .or. i/=yID) then
            k=len(trim(csvWord(i)))
            if (k>0) then
                local_no_column=i
                goto 1990
            end if
        end if
    end do
    
1990 i=0    
     
    
    do i=1,no_column
        if (i==xID .or. i==yID) then
        else
            k=len(trim(csvWord(i)))
            if (k>0) then
                QQ=0
                do j=1,k
                    ss=iachar(csvWord(i)(j:j))
                    if (ss>47 .and. ss<58 ) then
                        QQ=QQ+0
                    else if (ss==46) then
                        QQ=QQ+1
                    else if ((j==1 .and. ss/=pasc .and. ss/=masc) .or. (j>1 .and. ss/=Easc .and. ss/=eeasc .and. ss/=Dasc .and. ss/=ddasc .and. ss/=pasc .and. ss/=masc)) then
                        QQ=QQ+2
                        exit
                    end if
                end do
                
                if (i/=local_no_column) then
                    if (QQ==0) then
                        read(csvWord(i)(1:256),"(I)",IOSTAT=ierror) INB
                        write(20,"(A,I0,A)") '"'//trim(title(i))//'":',INB,','
                    else if(QQ==1) then
                        read(csvWord(i)(1:256),"(E)",IOSTAT=ierror) DNB
                        write(20,"(A,E,A)") '"'//trim(title(i))//'":',DNB,','
                    else if (QQ>=2) then
                        write(20,"(A,A,A)") '"'//trim(title(i))//'":"',trim(csvWord(i)),'",'
                    end if
                else
                    if (QQ==0) then
                        read(csvWord(i)(1:256),"(I)",IOSTAT=ierror) INB
                        write(20,*) '"'//trim(title(i))//'":',INB
                    else if(QQ==1) then
                        read(csvWord(i)(1:256),"(E)",IOSTAT=ierror) DNB
                        write(20,*) '"'//trim(title(i))//'":',DNB
                    else if (QQ>=2) then
                        write(20,"(A,A,A)") '"'//trim(title(i))//'":"',trim(csvWord(i)),'"'
                    end if
                end if
            end if        
        end if
    end do
    write(20,"(A8)") '      },'
    write(20,"(A13)") '"geometry": {'
    write(20,"(A24)") '        "type": "Point",'
    write(20,"(A24)") '        "coordinates": ['
    read(csvWord(xID)(1:256),*,IOSTAT=ierror) DNB
    write(20,*) DNB,','
    read(csvWord(yID)(1:256),*,IOSTAT=ierror) DNB
    write(20,*) DNB
    
    write(20,"(A9)") '        ]'
    write(20,"(A7)") '      }'
    
    if (Ni/=no_line) then
        write(20,"(A6)") '    },'
    else
        write(20,"(A5)") '    }'
    end if
    
end do

call geojsonend(20)

word=''
do i=1,no_column
    tempword=trim(csvWord(i))
    word=trim(word)//','//trim(csvWord(i))
end do

i=0

    
end
    
    
    